package com.bi.oncallcamunda.tasks;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class AlertService implements JavaDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(AlertService.class);

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        LOGGER.debug("Sending alert signal !");
    }

}
