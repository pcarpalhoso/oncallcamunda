package com.bi.oncallcamunda;

import org.camunda.bpm.engine.RuntimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class OncallcamundaApplication {

	@Autowired
	private RuntimeService runtimeService;

	public static void main(String[] args) {
		SpringApplication.run(OncallcamundaApplication.class, args);
	}

	@PostConstruct
	public void startProcess() {
		runtimeService.startProcessInstanceByKey("oncall");
	}

}
